<?php

namespace App\Controller;

use App\Entity\Score;
use InvalidArgumentException;
use Doctrine\ORM\EntityManager;
use App\Repository\ScoreRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Json;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ScoreController extends AbstractController
{
    #[Route('api/scores', name: 'api_scores_index', methods: ['GET'])]
    public function scores(ScoreRepository $scoreRepository): Response
    {
        return $this->json($scoreRepository->findAll(),200, [], ['groups' => 'scores:read']);
    }

    #[Route('api/scores', name: 'ajout-scores', methods: ['POST'])] 
   public function addScore(Request $request, SerializerInterface $serializer, EntityManagerInterface $em, 
    ValidatorInterface $validator)
   {
       
    $jsonRecu = $request->getContent();

    $score = $serializer->deserialize($jsonRecu, Score::class, 'json');

    $score->setCreatedAt(new \DateTimeImmutable());

    $errors = $validator->validate($score);

    if(count($errors) > 0){
        return $this->json($errors, 400);
    }

    $em->persist($score);
    $em->flush($score);

    return $this->json($score, 201, ['groups' =>'score:read']);
   }


   #[Route('api/scores/{id}', name: 'suppression-scores', methods: ['DELETE'])]

    public function deleteScore (EntityManagerInterface $em, ScoreRepository $scoreRepo, int $id){

        try {
            $score = $scoreRepo->findOneBy(['id' => $id]);
            $em->remove($score);
            $em->flush();
            return $this->json([
                'status' => 200,
                'message' => 'Score supprimé'
            ]);
        } catch(NotFoundHttpException $e) {
            return $this->json([
                'status' => 400,
                'message' => $e->getMessage()
            ]);
        } catch(InvalidArgumentException $e) {
            return $this->json([
                'status' => 400,
                'message' => $e->getMessage()
            ]);
        }
   }
}
