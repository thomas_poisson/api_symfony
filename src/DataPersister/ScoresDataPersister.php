<?php

// src/DataPersister

namespace App\DataPersister;

use App\Entity\Score;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;

class ScoreDataPersister implements ContextAwareDataPersisterInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $_entityManager;

    public function __construct(
        EntityManagerInterface $entityManager,
        RequestStack $request
    ) {
        $this->_entityManager = $entityManager;
        $this->_request = $request->getCurrentRequest();
    }

    /**
     * {@inheritdoc}
     */

    public function supports($data, array $context = []): bool
    {
        return $data instanceof Score;
    }

      /**
     * @param Score $data
     */

    public function persist($data, array $context = [])
    {
        if (!$data->getId()) {
            $data->setCreatedAt(new \DateTimeImmutable('now'));
        }
 
        $this->entityManager->persist($data);
        $this->entityManager->flush();
    }

    public function remove($data, array $context = [])
    {
        $this->_entityManager->remove($data);
        $this->_entityManager->flush();
    }
}