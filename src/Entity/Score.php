<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ScoreRepository;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ScoreRepository::class)
 * @ApiResource(
 *          normalizationContext={"groups"={"scores:read"}},
 *          denormalizationContext={"groups"={"scores:write"}}
 * )
 */

class Score
{


    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * 
     * @Groups("scores:read")
     *
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"scores:write", "scores:read"})
     * 
     * @Assert\NotBlank(message="Le pseudo est obligatoire")
     */
    private $Pseudo;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"scores:write", "scores:read"})
     * @Assert\NotBlank
     * 
     */
    private $Score;


    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     *
     * @Groups("scores:read")
     */
    private $createdAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPseudo(): ?string
    {
        return $this->Pseudo;
    }

    public function setPseudo(string $Pseudo): self
    {
        $this->Pseudo = $Pseudo;

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->Score;
    }

    public function setScore(int $Score): self
    {
        $this->Score = $Score;

        return $this;
    }

    public function __construct()
    {
        $this->isPublished = false;
        $this->createdAt = new \DateTimeImmutable();
    }


    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
